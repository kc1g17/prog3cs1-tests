# Programming III Coursework 1 Tests

This is a student-collaborated test repository for Programming III Coursework 1.

This repo contains a Tests.hs file which is the same as the one provided for the coursework but with additional test cases, including finnicky edge cases.

## Important notes

- This repo **is not** officially supported by the Programming III team. If there are any problems with this repo, please submit a GitLab issue. **Do not** expect staff to support your use of this repo.
- The contributions to this repo, except the original test cases, are **entirely developed by students**. This means **there may be mistakes**. These test are not guaranteed to be correct, so **do not** rely on them exclusively as a way of checking if your code works.
- Using these tests constitutes **collaboration** according to academic integrity. Therefore **you must** declare that you have used these tests on handin. An example way of doing this is provided below.
- You **must not** use this repository to in any way violate academic integrity, including but not limited to showing Exercises.hs code to other students.

## Academic integrity declaration

You **must** declare your usage of these test on the handin. Here is an example way of doing so:

```
I have collaborated with other students in making a shared repository of tests. This repository consists of an extended Tests.hs file with more test cases, especially with the intention of identifying and testing edge cases. This repository does not contain any Exercises.hs code, and does not include the sharing of any Exercises.hs code; it only consists of test cases and discussion thereof. The repository can be found at https://gitlab.com/kc1g17/prog3cs1-tests
```

## Collaboration

If you wish to collaborate to this repo, the best way is to use **GitLab merge requests and issues**. If you have spotted a problem with a test, submit an issue. If you have a new test, or a solution to a problem with a test, submit a merge request. Take care not to violate academic integrity during this process (e.g. saying that a test doesn't work on your breath-first solution is fine, saying that a test doesn't work on your solution and posting the relevant Exercises.hs code snipped is not fine).
